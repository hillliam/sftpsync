using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using sftpsync.SFTP;

namespace sftpsync.SSHServer
{
    public class TcpForwardService
    {
        private readonly string _host;
        private readonly int _port;
        private bool _connected;
        private List<byte> _blocked;
        private readonly ILogger<TcpForwardService> Logger;
        private readonly ITcpFtp TcpFtp;

        public TcpForwardService(string host, int port, string originatorIP, int originatorPort, ILogger<TcpForwardService> logger, ITcpFtp tcpFtp)
        {
            _host = host;
            _port = port;
            Logger = logger;
            TcpFtp = tcpFtp;
            _connected = false;
            _blocked = new List<byte>();
        }

        public event EventHandler<byte[]> DataReceived;
        public event EventHandler CloseReceived;

        public void Start()
        {
            Task.Run(() =>
            {
                try
                {
                    MessageLoop();
                }
                catch
                {
                    OnClose();
                }
            });
        }

        public void OnData(byte[] data)
        {
            try
            {
                if (_connected)
                {
                    if (_blocked.Count > 0)
                    {
                        TcpFtp.Send(_blocked.ToArray());
                        _blocked.Clear();
                    }
                    TcpFtp.Send(data);
                }
                else
                {
                    _blocked.AddRange(data);
                }
            }
            catch
            {
                OnClose();
            }
        }

        public void OnClose()
        {
            try
            {
                TcpFtp.Shutdown();
            }
            catch { }
        }

        private void MessageLoop()
        {
            TcpFtp.Connect(_host, _port);
            _connected = true;
            OnData(Array.Empty<byte>());
            var bytes = new byte[1024 * 64];
            while (true)
            {
                var len = TcpFtp.Receive(bytes);
                if (len <= 0)
                    break;

                var data = bytes.Length != len
                    ? bytes.Take(len).ToArray()
                    : bytes;
                DataReceived?.Invoke(this, data);
            }
            CloseReceived?.Invoke(this, EventArgs.Empty);
        }
    }
}