namespace sftpsync.SFTP
{
    public interface ITcpFtp
    {
        void Send(byte[] data);
        void Connect(string host, int port);
        void Shutdown();
        int Receive(byte[] data);
    }
}