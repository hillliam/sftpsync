using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FxSsh;
using FxSsh.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using sftpsync.SFTP;

namespace sftpsync.SSHServer
{
    public class SSHServer : ISSHServer
    {
        private readonly ILogger<SSHServer> Logger;
        private readonly ILogger<TcpForwardService> tcpLogger;
        private readonly IConfiguration Configuration;
        private readonly SshServer Server;
        private readonly ITcpFtp tcpFtp;
        
        private int windowWidth, windowHeight;

        public SSHServer(ILogger<SSHServer> logger, IConfiguration configuration, SshServer server, ILogger<TcpForwardService> tcpLogger, ITcpFtp tcpFtp)
        {
            Logger = logger;
            Configuration = configuration;
            Server = server;
            this.tcpLogger = tcpLogger;
            this.tcpFtp = tcpFtp;
            server.ExceptionRasied += server_Error;
            server.ConnectionAccepted += server_ConnectionAccepted;

            server.Start();
        }

        private void server_Error(object sender, Exception e)
        {
            Logger.LogError("ssh error", e);
        }

        private void server_ConnectionAccepted(object sender, Session e)
        {
            Logger.LogDebug("Accepted a client.");

            e.ServiceRegistered += e_ServiceRegistered;
            e.KeysExchanged += e_KeysExchanged;
        }

        private void e_KeysExchanged(object sender, KeyExchangeArgs e)
        {
            foreach (var keyExchangeAlg in e.KeyExchangeAlgorithms)
            {
                Logger.LogDebug("Key exchange algorithm: {0}", keyExchangeAlg);
            }
        }

        private void e_ServiceRegistered(object sender, SshService e)
        {
            var session = (Session)sender;
            Logger.LogDebug("Session {0} requesting {1}.",
                BitConverter.ToString(session.SessionId).Replace("-", ""), e.GetType().Name);

            if (e is UserauthService)
            {
                var service = (UserauthService)e;
                service.Userauth += service_Userauth;
            }
            else if (e is ConnectionService)
            {
                var service = (ConnectionService)e;
                service.CommandOpened += service_CommandOpened;
                service.EnvReceived += service_EnvReceived;
                service.PtyReceived += service_PtyReceived;
                service.TcpForwardRequest += service_TcpForwardRequest;
            }
        }

        private void service_TcpForwardRequest(object sender, TcpRequestArgs e)
        {
            Logger.LogDebug("Received a request to forward data to {0}:{1}", e.Host, e.Port);
            
            var tcp = new TcpForwardService(e.Host, e.Port, e.OriginatorIP, e.OriginatorPort, tcpLogger, tcpFtp);
            e.Channel.DataReceived += (ss, ee) => tcp.OnData(ee);
            e.Channel.CloseReceived += (ss, ee) => tcp.OnClose();
            tcp.DataReceived += (ss, ee) => e.Channel.SendData(ee);
            tcp.CloseReceived += (ss, ee) => e.Channel.SendClose();
            tcp.Start();
        }

        private void service_PtyReceived(object sender, PtyArgs e)
        {
            Logger.LogDebug("Request to create a PTY received for terminal type {0}", e.Terminal);
            windowWidth = (int)e.WidthChars;
            windowHeight = (int)e.HeightRows;
        }

        private void service_EnvReceived(object sender, EnvironmentArgs e)
        {
            Logger.LogDebug("Received environment variable {0}:{1}", e.Name, e.Value);
        }

        private void service_Userauth(object sender, UserauthArgs e)
        {
            Logger.LogDebug("Client {0} fingerprint: {1}.", e.KeyAlgorithm, e.Fingerprint);
            
            e.Result = true;
        }

        private void service_CommandOpened(object sender, CommandRequestedArgs e)
        {
            Logger.LogDebug($"Channel {e.Channel.ServerChannelId} runs {e.ShellType}: \"{e.CommandText}\".");
            
        }
    }
}