using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using sftpsync.SSHServer;

namespace sftpsync.Workers
{
    public class SSHTunnelWorker : BackgroundService
    {
        private readonly ILogger<SSHTunnelWorker> _logger;
        private readonly ISSHServer SshServer;

        public SSHTunnelWorker(ILogger<SSHTunnelWorker> logger, ISSHServer sshServer)
        {
            _logger = logger;
            SshServer = sshServer;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}