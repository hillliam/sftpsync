using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using sftpsync.model;

namespace sftpsync.Workers
{
    public class FolderSyncWorker : BackgroundService
    {
        private readonly ILogger<FolderSyncWorker> _logger;
        private readonly ISftpClient SftpClient;
        private readonly SyncSettings SyncSettings;
        private readonly bool server;
        private DateTime? lastSync;

        public FolderSyncWorker(ILogger<FolderSyncWorker> logger, SyncSettings syncSettings, ISftpClient sftpClient, IConfiguration configuration)
        {
            _logger = logger;
            SyncSettings = syncSettings;
            SftpClient = sftpClient;
            server = bool.Parse(configuration["Server"]);
            if (!server)
            {
                Client();
            }
        }
        

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (server)
                {
                    ServerLoop();
                }

                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(10, stoppingToken);
            }
        }

        // server src is ftp dst is local
        private void ServerLoop()
        {
            foreach (var syncSettingsSyncFolder in SyncSettings.SyncFolders)
            {
                var list = SftpClient.ListDirectory(syncSettingsSyncFolder.src);
                if (lastSync.HasValue)
                {
                    list = list.Where(a => a.LastWriteTimeUtc > this.lastSync.Value);
                }
                foreach (var sftpFile in list)
                {
                    SftpClient.DownloadFile(sftpFile.FullName, new FileStream(Subpath(sftpFile.FullName), FileMode.Create));
                }
            }
        }

        //client src is local dst is ftp
        private void Client()
        {
            foreach (var syncSettingsSyncFolder in SyncSettings.SyncFolders)
            {
                var watcher = new FileSystemWatcher();
                watcher.IncludeSubdirectories = true;
                watcher.NotifyFilter = System.IO.NotifyFilters.DirectoryName;
                watcher.NotifyFilter = watcher.NotifyFilter | System.IO.NotifyFilters.FileName;
                watcher.NotifyFilter = watcher.NotifyFilter | System.IO.NotifyFilters.Attributes;
                
                watcher.Changed += new FileSystemEventHandler(eventChangeRaised);
                watcher.Created += new FileSystemEventHandler(eventCreateRaised);
                watcher.Deleted += new FileSystemEventHandler(eventDeleteRaised);
                
                watcher.Renamed += new System.IO.RenamedEventHandler(eventRenameRaised);
                
                try
                {
                    watcher.EnableRaisingEvents = true;
                }
                catch (ArgumentException ex)
                {
                    _logger.LogError("File Synchronization Service not start: ", ex);
                }
            }
        }

        private string Subpath(string path)
        {
            foreach (var syncSettingsSyncFolder in SyncSettings.SyncFolders)
            {
                if (!server && path.StartsWith(syncSettingsSyncFolder.src))
                {
                    return Path.Combine(syncSettingsSyncFolder.dst, path.Substring(syncSettingsSyncFolder.src.Length));
                }
                else if (server && path.StartsWith(syncSettingsSyncFolder.src))
                {
                    return Path.Combine(syncSettingsSyncFolder.src, path.Substring(syncSettingsSyncFolder.dst.Length));
                }
            }

            return path;
        }

        private void eventChangeRaised(object sender, System.IO.FileSystemEventArgs e)
        {
            var path = e.FullPath;
            var stream = SftpClient.Create(Subpath(path));
            var file = new FileStream(path, FileMode.Open);
            file.CopyTo(stream);
            stream.Close();
        }

        private void eventCreateRaised(object sender, System.IO.FileSystemEventArgs e)
        {
            var path = e.FullPath;
            var stream = SftpClient.Create(Subpath(path));

            var file = new FileStream(path, FileMode.Open);
            file.CopyTo(stream);
            stream.Close();
        }
 
        private void eventDeleteRaised(object sender, System.IO.FileSystemEventArgs e)
        {
             var path = e.FullPath;
             SftpClient.DeleteFile(e.FullPath);
        }
  
        private void eventRenameRaised(object sender, System.IO.RenamedEventArgs e)
        {
              var path = e.FullPath;
              var oldpath = e.OldFullPath;
              SftpClient.RenameFile(oldpath, path);
        }
    }
}
