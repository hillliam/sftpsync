using System.Net;
using FxSsh;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Renci.SshNet;
using sftpsync.model;
using sftpsync.SFTP;
using sftpsync.SSHServer;
using sftpsync.Workers;

namespace sftpsync
{
    public static class startup
    {

        public static void configureServices(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var ftpServer = configuration["ftpServer"];
            var ftpUsername = configuration["ftpUsername"];
            var ftpPassword = configuration["ftpPassword"];

            var sshPort = int.Parse(configuration["sshPort"]);
            var sshBanner = configuration["sshBanner"];

            var server = bool.Parse(configuration["Server"]);

            var connectionInfo = new PasswordConnectionInfo(ftpServer, ftpUsername, ftpPassword);
            var startinginfo = new StartingInfo(IPAddress.Loopback, sshPort, sshBanner);

            var syncSetting = new SyncSettings();
            configuration.GetSection("FTPSync").Bind(syncSetting);

            serviceCollection.AddTransient(a => syncSetting);
            serviceCollection.AddTransient<ConnectionInfo>(a => connectionInfo);
            serviceCollection.AddTransient(a => startinginfo);
            serviceCollection.AddTransient<ISftpClient, SftpClient>();
            serviceCollection.AddTransient<SshServer>();
            serviceCollection.AddTransient<ISSHServer, SSHServer.SSHServer>();
            serviceCollection.AddTransient<TcpForwardService>();
            serviceCollection.AddTransient<TCPSenderService>();
            serviceCollection.AddTransient<ITcpFtp, TcpFtp>();
            serviceCollection.AddTransient<IConnectionManager, ConnectionManager>();

            serviceCollection.AddHostedService<FolderSyncWorker>();
            serviceCollection.AddHostedService<SSHTunnelWorker>();
            if (server)
            {
                serviceCollection.AddHostedService<TCPForwardWorker>();
            }

        }
    }
}