namespace sftpsync.model
{
    public sealed  class SyncSettings
    {
        public SyncFolder[] SyncFolders { get; set; }
    }

    public sealed  class SyncFolder
    {
        public string src { get; set; }
        public string dst { get; set; }
    }
}