using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using sftpsync.SSHServer;

namespace sftpsync.Workers
{
    public class TCPForwardWorker : BackgroundService
    {
        private readonly ILogger<TCPForwardWorker> _logger;
        private readonly ISftpClient sftpClient;
        private readonly string folder;
        private List<TCPSenderService> Services = new List<TCPSenderService>();

        public TCPForwardWorker(ILogger<TCPForwardWorker> logger, ISftpClient sftpClient, IConfiguration configuration)
        {
            _logger = logger;
            this.sftpClient = sftpClient;
            folder = configuration["tcpFtpFolder"];
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var list = this.sftpClient.ListDirectory(folder);
                foreach (var sftpFile in list)
                {
                    var info = sftpFile.Name.Split('|');
                    var host = info[0];
                    var port = int.Parse(info[1]);
                    var direction = info[2];
                    var content = sftpClient.ReadAllBytes(sftpFile.FullName);
                    if (direction == "tx")
                    {// data to send
                        //check for ready file
                        while (!sftpClient.Exists(sftpFile.FullName + "Ready"))
                        {
                            
                        }
                        if (Services.Exists(a => a.Host == host && a.Port == port))
                        { // send new data
                            var service = Services.Find(a => a.Host == host && a.Port == port);
                            service.SendData(content);
                        }
                        else
                        {// new port mapping and set up data recevied event
                            var service = new TCPSenderService(host, port);
                            Services.Add(service);
                            service.DataReceived += DataRecevied; 
                            service.SendData(content);
                            service.Start();
                        }
                        sftpClient.DeleteFile(sftpFile.FullName);
                        sftpClient.DeleteFile(sftpFile.FullName + "Ready");
                    }
                }
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1, stoppingToken);
            }
        }

        private void DataRecevied(object sender, byte[] data)
        {
            if (sender is TCPSenderService service)
            {
                var host = service.Host;
                var port = service.Port;
                _logger.LogDebug("", host, port, data.Length);
                var stream = sftpClient.Create(Path.Combine(folder, host + "|" + port + "|rx" ));
                stream.Write(data, 0, data.Length);
                stream.Close();
                sftpClient.Create(Path.Combine(folder, host + port + "rxReady")).Close();
            }
        }
    }
}