using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Renci.SshNet;

namespace sftpsync.SFTP
{
    public class TcpFtp : ITcpFtp
    {
        private readonly ILogger<TcpFtp> Logger;
        private readonly ISftpClient SftpClient;
        private readonly string folder;
        private string host;
        private int port;

        public TcpFtp(ILogger<TcpFtp> logger, ISftpClient sftpClient, IConfiguration configuration)
        {
            Logger = logger;
            SftpClient = sftpClient;
            folder = configuration["tcpFtpFolder"];
        }

        public void Send(byte[] data)
        {
            Logger.LogDebug("file created " + Path.Combine(folder, host + "|" + port + "|" + "tx"));
            var stream = this.SftpClient.Create(Path.Combine(folder, host + "|" + port + "|" + "tx"));
            stream.Write(data, 0, data.Length);
            stream.Close();
            this.SftpClient.Create(Path.Combine(folder, host + "|" + port + "|" + "txReady")).Close();
            Logger.LogDebug("file created " + Path.Combine(folder, host + "|" + port + "|" + "txReady"));
        }

        public void Connect(string host, int port)
        {
            this.host = host;
            this.port = port;
        }

        public void Shutdown()
        {
            Logger.LogDebug("file deleted " + Path.Combine(folder, host + "|" + port + "|" + "tx"));
            Logger.LogDebug("file deleted " + Path.Combine(folder, host + "|" + port + "|" + "txReady"));
            this.SftpClient.Delete(Path.Combine(folder, host + "|" + port + "|" + "tx"));
            this.SftpClient.Delete(Path.Combine(folder, host + "|" + port + "|" + "txReady"));
        }

        public int Receive(byte[] data)
        {
            var stream = new MemoryStream();
            while (!this.SftpClient.Exists(Path.Combine(folder, host + "|" + port + "|" + "RxReady")))
            {
                Logger.LogDebug("waiting for file " + Path.Combine(folder, host + "|" + port + "|" + "RxReady"));
            }
            this.SftpClient.DeleteFile(Path.Combine(folder, host + "|" + port + "|" + "RxReady"));
            this.SftpClient.DownloadFile(Path.Combine(folder, host + "|" + port + "|" + "Rx"), stream);
            stream.Read(data, 0, (int)stream.Length);
            Logger.LogDebug("read from file " + stream.Length);
            return (int) stream.Length;
        }
    }
}